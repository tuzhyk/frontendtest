import React, { Component } from "react";
import SimpleList from "./containers/list";
import "./App.css";
import { BrowserRouter as Router, Route } from "react-router-dom";
import UserPage from "./components/userPage";
import { createStore } from "redux";
import { Provider } from "react-redux";
import reducer from "./reducers";
import * as dataa from "./data.json";

const initialState = {
  list: dataa.Brastlewark,
  displayList: dataa.Brastlewark,
  filter: "",
  showFrom: 0,
  count: 50
};
const store = createStore(reducer, initialState);

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <div className="App">
            <Route path="/" exact component={SimpleList} />
            <Route path="/user/" component={UserPage} />
          </div>
        </Router>
      </Provider>
    );
  }
}

export default App;
