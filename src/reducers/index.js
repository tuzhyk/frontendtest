import {
  SET_FILTER,
  DELETE_FILTER,
  CHANGE_DISPLAY_LIST,
  SHOW_FROM,
  CHANGE_COUNT,
} from "../constants/index";
const initialState = {};

export default function todos(state = initialState, action) {
  switch (action.type) {
    case SET_FILTER:
      return { ...state, filter: action.filter };

    case DELETE_FILTER:
      return { ...state, filter: {} };

    case CHANGE_DISPLAY_LIST:
      return { ...state, displayList: action.displayList };

    case CHANGE_COUNT:
      return { ...state, count: action.count };

    case SHOW_FROM:
      return { ...state, showFrom: action.showFromChangeProp };

    default:
      return state;
  }
}
