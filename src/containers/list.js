import { connect } from 'react-redux';
import List from '../components/list';
import {
  setFilter,
  deleteFilter,
  changeDisplayList,
  changeCount,
  showFromChange,
} from '../actions';
const mapStateToProps = state => ({
  filter: state.filter,
  displayList: state.displayList,
  list: state.list,
  showFrom: state.showFrom,
  count: state.count,
});
//   const mapDispatchToProps = (dispatch) => ({
//     setFilter: (filter) => {
//       dispatch(setFilter(filter))
//     }
//   })
export default connect(
  mapStateToProps,
  { setFilter, deleteFilter, changeDisplayList, changeCount, showFromChange }
)(List);
