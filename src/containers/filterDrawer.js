import { connect } from "react-redux";
import FilterDrawer from "../components/filterDrawer";
import {
  setFilter,
  deleteFilter,
  changeDisplayList,
  changeCount,
  showFromChange,
} from "../actions";
const mapStateToProps = state => ({
  filter: state.filter,
  displayList: state.displayList,
  list: state.list,
  showFrom: state.showFrom,
  count: state.count,
});
export default connect(
  mapStateToProps,
  { setFilter, deleteFilter, changeDisplayList, changeCount, showFromChange }
)(FilterDrawer);
