import {
  CHANGE_DISPLAY_LIST,
  SET_FILTER,
  DELETE_FILTER,
  CHANGE_COUNT,
  SHOW_FROM
} from "../constants/index";

export const changeDisplayList = displayList => ({
  type: CHANGE_DISPLAY_LIST,
  displayList
});
export const setFilter = filter => ({ type: SET_FILTER, filter });
export const deleteFilter = () => ({ type: DELETE_FILTER });
export const changeCount = count => ({ type: CHANGE_COUNT, count });
export const showFromChange = showFromChangeProp => ({
  type: SHOW_FROM,
  showFromChangeProp
});
