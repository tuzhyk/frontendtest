import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import GridList from "@material-ui/core/GridList";
import GridListTile from "@material-ui/core/GridListTile";
import GridListTileBar from "@material-ui/core/GridListTileBar";
import ListSubheader from "@material-ui/core/ListSubheader";
import { withRouter } from "react-router-dom";
import Filter from "../containers/filterDrawer";
import NavPrev from "@material-ui/icons/NavigateBefore";
import NavNext from "@material-ui/icons/NavigateNext";
import IconButton from "@material-ui/core/IconButton";
//in other file
const styles = theme => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-around",
    overflow: "hidden",
    backgroundColor: theme.palette.background.paper
  },
  gridList: {
    justifyContent: "center"
  },
  gridListHeader: {
    height: "auto",
    width: "90%"
  }
});

class List extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false
    };
  }

  handleClickOpen = () => {
    this.setState({
      open: true
    });
  };

  search = value => {
    const regexp = new RegExp(value, "gi");
    const newList = this.props.displayList.filter(item => {
      if (item.name.search(regexp) !== -1) return true;
      return false;
    });
    this.props.showFromChange(0);
    this.props.changeDisplayList(newList);
  };

  handleChangeSearch = e => {
    this.search(e.target.value);
  };

  handleClickOpen = () => {
    this.setState({
      open: true
    });
  };

  Filtration = filter => {
    const {
      ageStart,
      ageEnd,
      heightStart,
      heightEnd,
      weightStart,
      weightEnd,
      friendsStart,
      friendsEnd
    } = filter;
    const newList = this.props.list.filter(item => {
      if (ageStart) {
        if (item.age < ageStart) {
          return false;
        }
      }
      if (ageEnd) {
        if (item.age > ageEnd) return false;
      }
      if (friendsStart) {
        if (item.friends.length < friendsStart) {
          return false;
        }
      }
      if (friendsEnd) {
        if (item.friends.length > friendsEnd) {
          return false;
        }
      }
      if (weightStart) {
        if (item.weight < weightStart) {
          return false;
        }
      }
      if (weightEnd) {
        if (item.weight > weightEnd) {
          return false;
        }
      }
      if (heightStart) {
        if (item.height < heightStart) {
          return false;
        }
      }
      if (heightEnd) {
        if (item.height > heightEnd) {
          return false;
        }
      }
      return true;
    });
    this.props.showFromChange(0);
    this.props.changeDisplayList(newList);
  };

  handleClose = filter => {
    this.setState({
      open: false
    });
    if (filter !== this.props.filter) {
      this.props.setFilter(filter);
      this.Filtration(filter);
    }
  };

  prevPage = () => {
    const nextShowFrom = this.props.showFrom - this.props.count;
    if (!(nextShowFrom <= 0)) this.props.showFromChange(nextShowFrom);
  };

  nextPage = () => {
    const nextShowFrom = this.props.showFrom + this.props.count;
    if (!(nextShowFrom >= this.props.displayList.length - 2))
      this.props.showFromChange(nextShowFrom);
  };

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <div>
          <Filter onClose={this.handleClose} />
          <GridList cellHeight={150} spacing={20} className={classes.gridList}>
            <GridListTile
              key="Subheader"
              classes={{ root: classes.gridListHeader }}
              style={{ height: "auto", width: "20%" }}
            >
              <IconButton onClick={this.prevPage} aria-label="Delete" style={{ float:'left' }}>
                <NavPrev style={{ width: "50px", height: "auto" }} />
              </IconButton>
            </GridListTile>
            <GridListTile
              key="Subheader"
              classes={{ root: classes.gridListHeader }}
              style={{ height: "auto", width: "60%" }}
            >
              <ListSubheader
                style={{ fontSize: "200%", paddingTop: "15px" }}
                component="div"
              >
                SomeCity
              </ListSubheader>
            </GridListTile>
            <GridListTile
              key="Subheader"
              classes={{ root: classes.gridListHeader }}
              style={{ height: "auto", width: "20%" }}
            >
              <IconButton onClick={this.nextPage} aria-label="Delete" style={{ float:'right' }}>
                <NavNext style={{ width: "50px", height: "auto" }} />
              </IconButton>
            </GridListTile>
            {/* in function */}
            {this.props.displayList
              .slice(
                this.props.showFrom,
                this.props.showFrom + this.props.count
              )
              .map((item, i) => (
                <div
                  key={item.id}
                  style={{ width: "none" }}
                  onClick={() => {
                    this.props.history.push({
                      pathname: `/user/${item.id}`,
                      state: {
                        user: item,
                        prevState: this.state //??? change li store ofter go away
                      }
                    });
                  }}
                >
                  <GridListTile style={{ width: "none" }}>
                    <img
                      src={item.thumbnail}
                      alt={item.name}
                      style={{ width: "150px", height: "150px" }}
                    />
                    <GridListTileBar 
                    style={{marginBottom: '3px'}}
                      title={item.name}
                      subtitle={<span>{item.friends.length} friends</span>}
                    />
                  </GridListTile>
                </div>
              ))}
          </GridList>
        </div>
      </div>
    );
  }
}

List.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withRouter(withStyles(styles)(List));
