import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";

const styles = theme => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-around",
    overflow: "hidden",
    backgroundColor: theme.palette.background.paper
  },
  gridList: {
    justifyContent: "center"
  },
  wrapped: {
    margin: "30px"
  },
  avatar: {
    height: "auto",
    width: "auto",
    maxWidth: "80%",
    maxHeight: "50vh"
  },
  paramValue: {
    display: "unset"
  }
});

function SimpleList(props) {
  const { classes } = props;
  const user = props.location.state.user;
  return (
    <div>
      <div className={classes.wrapped}>
        <img src={user.thumbnail} alt={user.name} className={classes.avatar} />
        <Typography variant="h4" component="div">
          Name:
          <Typography
            className={classes.paramValue}
            variant="h5"
            component="span"
          >
            {user.name}
          </Typography>
        </Typography>
        <Typography variant="h4" component="div">
          Age:
          <Typography
            className={classes.paramValue}
            variant="h5"
            component="span"
          >
            {user.age}
          </Typography>
        </Typography>
        <Typography variant="h4" component="div">
          Weight:
          <Typography
            className={classes.paramValue}
            variant="h5"
            component="span"
          >
            {user.weight}
          </Typography>
        </Typography>
        <Typography variant="h4" component="div">
          Height:
          <Typography
            className={classes.paramValue}
            variant="h5"
            component="span"
          >
            {user.height}
          </Typography>
        </Typography>
        <Typography variant="h4" component="div">
          Hair color:
          <Typography
            className={classes.paramValue}
            variant="h5"
            component="span"
          >
            {user.hair_color}
          </Typography>
        </Typography>
        <Typography variant="h4" component="div">
          Proffesions:
          <Typography
            className={classes.paramValue}
            variant="h5"
            component="span"
          >
            {user.professions.map(item => `${item}, `)}
          </Typography>
        </Typography>

        <Typography variant="h4" component="h2">
          Friends:
          <Typography
            className={classes.paramValue}
            variant="h5"
            component="span"
          >
            {user.friends.map(item => `${item}, `)}
          </Typography>
        </Typography>
        <Button
          variant="contained"
          onClick={() => {
            props.history.push({
              pathname: "/",
              state: {
                prevState: props.location.state.prevState
              }
            });
          }}
        >
          back
        </Button>
      </div>
    </div>
  );
}

SimpleList.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(SimpleList);
