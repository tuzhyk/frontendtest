import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import SwipeableDrawer from "@material-ui/core/SwipeableDrawer";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import IconButton from "@material-ui/core/IconButton";
import Tune from "@material-ui/icons/Tune";

const styles = {
  list: {
    width: 250
  },
  fullList: {
    width: "auto"
  },
  textField: {
    width: 100,
    margin: 10
  },
  filterButton: {
    marginTop: 25
  }
};

class SwipeableTemporaryDrawer extends React.Component {
  constructor(props) {
    super(props);
    this.state = { filter: this.props.filter, right: false };
  }

  toggleDrawer = (side, open) => () => {
    this.setState({
      [side]: open
    });
  };

  handleClose = () => {
    this.props.onClose(this.state.filter);
    this.props.setFilter(this.state.filter);
    this.setState({
      right: false
    });
  };

  handleChange = name => event => {
    this.setState({
      filter: { ...this.state.filter, [name]: event.target.value }
    });
  };

  search = value => {
    const regexp = new RegExp(value, "gi");
    const newList = this.props.list.filter(item => {
      if (item.name.search(regexp) !== -1) return true;
      return false;
    });
    this.props.showFromChange(0);
    this.props.changeDisplayList(newList);
  };

  handleChangeSearch = e => {
    this.search(e.target.value);
  };

  resetFilter = () => {
    this.setState({ filter: "" });
  };

  render() {
    const { classes } = this.props;
    const iOS = process.browser && /iPad|iPhone|iPod/.test(navigator.userAgent);
    return (
      <div>
        <TextField
          id="standard-name"
          label="Search"
          onChange={this.handleChangeSearch}
          margin="normal"
          style={{ margin: "10px" }}
        />
        <IconButton
          className={classes.filterButton}
          onClick={this.toggleDrawer("right", true)}
          aria-label="Delete"
        >
          <Tune />
        </IconButton>
        <SwipeableDrawer
          anchor="right"
          open={this.state.right}
          onClose={this.handleClose}
          onOpen={this.toggleDrawer("right", true)}
          disableBackdropTransition={!iOS}
          disableDiscovery={iOS}
          disableSwipeToOpen={false}
        >
          <div className={classes.list}>
            <div style={{ textAlign: "center", justifyContent: "center" }}>
              <Typography variant="h5" component="h2">
                Age
              </Typography>
              <TextField
                id="standard-name"
                label="Start age"
                className={classes.textField}
                value={this.state.filter.ageStart || ""}
                onChange={this.handleChange("ageStart")}
                margin="normal"
              />
              <TextField
                id="standard-name"
                label="End age"
                className={classes.textField}
                value={this.state.filter.ageEnd || ""}
                onChange={this.handleChange("ageEnd")}
                margin="normal"
              />
              <Typography variant="h5" component="h2">
                Weight
              </Typography>
              <TextField
                id="standard-name"
                label="start weight"
                className={classes.textField}
                value={this.state.filter.weightStart || ""}
                onChange={this.handleChange("weightStart")}
                margin="normal"
              />
              <TextField
                id="standard-name"
                label="end weight"
                className={classes.textField}
                value={this.state.filter.weightEnd || ""}
                onChange={this.handleChange("weightEnd")}
                margin="normal"
              />
              <Typography variant="h5" component="h2">
                height
              </Typography>
              <TextField
                id="standard-name"
                label="start height"
                className={classes.textField}
                value={this.state.filter.heightStart || ""}
                onChange={this.handleChange("heightStart")}
                margin="normal"
              />
              <TextField
                id="standard-name"
                label="end height"
                className={classes.textField}
                value={this.state.filter.heightEnd || ""}
                onChange={this.handleChange("heightEnd")}
                margin="normal"
              />
              <Typography variant="h5" component="h2">
                friends
              </Typography>
              <TextField
                id="standard-name"
                label="friends start"
                className={classes.textField}
                value={this.state.filter.friendsStart || ""}
                onChange={this.handleChange("friendsStart")}
                margin="normal"
              />
              <TextField
                id="standard-name"
                label="friends end"
                className={classes.textField}
                value={this.state.filter.friendsEnd || ""}
                onChange={this.handleChange("friendsEnd")}
                margin="normal"
              />
              <Button onClick={this.resetFilter}>reset</Button>
            </div>
          </div>
        </SwipeableDrawer>
      </div>
    );
  }
}

SwipeableTemporaryDrawer.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(SwipeableTemporaryDrawer);
