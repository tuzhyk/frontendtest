import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import DialogTitle from "@material-ui/core/DialogTitle";
import Dialog from "@material-ui/core/Dialog";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";

const styles = {
};

class SimpleDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = this.props.filter;
  }

  handleClose = () => {
    this.props.onClose(this.state);
  };

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value
    });
  };

  handleListItemClick = value => {
    this.props.onClose(value);
  };

  render() {
    const { classes, onClose, selectedValue, ...other } = this.props;

    return (
      <Dialog
        onClose={this.handleClose}
        aria-labelledby="simple-dialog-title"
        {...other}
        style={{ textAlign: "center", justifyContent: "center" }}
      >
        <DialogTitle id="simple-dialog-title">Filter</DialogTitle>
        <div style={{ textAlign: "center", justifyContent: "center" }}>
          <Typography variant="h5" component="h2">
            Age
          </Typography>
          <TextField
            id="standard-name"
            label="Start age"
            className={classes.textField}
            value={this.state.ageStart}
            onChange={this.handleChange("ageStart")}
            margin="normal"
            style={{ margin: "10px" }}
          />
          <TextField
            id="standard-name"
            label="End age"
            className={classes.textField}
            value={this.state.ageEnd}
            onChange={this.handleChange("ageEnd")}
            margin="normal"
            style={{ margin: "10px" }}
          />
          <Typography variant="h5" component="h2">
            Weight
          </Typography>
          <TextField
            id="standard-name"
            label="start weight"
            className={classes.textField}
            value={this.state.weightStart}
            onChange={this.handleChange("weightStart")}
            margin="normal"
            style={{ margin: "10px" }}
          />
          <TextField
            id="standard-name"
            label="end weight"
            className={classes.textField}
            value={this.state.weightEnd}
            onChange={this.handleChange("weightEnd")}
            margin="normal"
            style={{ margin: "10px" }}
          />
          <Typography variant="h5" component="h2">
            height
          </Typography>
          <TextField
            id="standard-name"
            label="start height"
            className={classes.textField}
            value={this.state.heightStart}
            onChange={this.handleChange("heightStart")}
            margin="normal"
            style={{ margin: "10px" }}
          />
          <TextField
            id="standard-name"
            label="end height"
            className={classes.textField}
            value={this.state.heightEnd}
            onChange={this.handleChange("heightEnd")}
            margin="normal"
            style={{ margin: "10px" }}
          />
          <Typography variant="h5" component="h2">
            friends
          </Typography>
          <TextField
            id="standard-name"
            label="friends start"
            className={classes.textField}
            value={this.state.friendsStart}
            onChange={this.handleChange("friendsStart")}
            margin="normal"
            style={{ margin: "10px" }}
          />
          <TextField
            id="standard-name"
            label="friends end"
            className={classes.textField}
            value={this.state.friendsEnd}
            onChange={this.handleChange("friendsEnd")}
            margin="normal"
            style={{ margin: "10px" }}
          />
          <button onClick={() => this.handleListItemClick(this.state)} />
        </div>
      </Dialog>
    );
  }
}

SimpleDialog.propTypes = {
  classes: PropTypes.object.isRequired,
  onClose: PropTypes.func,
  selectedValue: PropTypes.string
};

export default withStyles(styles)(SimpleDialog);
